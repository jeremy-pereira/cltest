# CLTest

This package is just for testing good Swift practices for command line tools. However, it might have some useful applications.

## Signals

The only safe way to do signal handling in Swift is to use a signal dispatch source. This, in turn means defining a run loop and using it, which is not easy if your command line uses traditional flow and not events.

The `Signal` enumeration provides a simple way to monitor signals and find out if they have been raised. You cannot set a custom signal handler, so it's fairly limited. However, in cases where you just want a flag that says a signal has been raised, it will do fine.

Here is a simple program that illustrates the use of the type. This monitors `SIGINT` which is raised when you press <kbd>ctrl</kbd>-<kbd>c</kbd> and exits the loop when it is.

```
do
{
	try Signal.int.monitor()
	for i in 0 ..< Int.max
	{
		guard !Signal.int.wasRaised() else { break }
		if i % 1000000 == 0
		{
			print("Iterated \(i) times")
		}
	}
	print("\nCaught ctrl-c and Terminated")
}
catch
{
	print("\(error)")
}

```
The underlying implementation uses `atomic_flag` which is a C11 atomic boolean type. Calling `wasRaised()` should therefore be thread safe and synchronised even across multiple cores, although the rest of the API is not thread safe.

As of version 0.2.0, an alternative closure based API is available.

```
try Signal.int.monitor
{
	for i in 0 ..< Int.max
	{
		guard !$0.wasRaised() else { break }
		if i % 1000000 == 0
		{
			print("Iterated \(i) times")
		}
	}
}
print("\nCaught ctrl-c and Terminated")

```
In this case, the signal is being monitored for the entire time the closure is being executed and monitoring stops when the closure's scope is exited, unless the signal was already being monitored.
