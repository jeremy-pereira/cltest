//
//  sighandler.c
//  
//
//  Created by Jeremy Pereira on 13/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "sighandler.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdatomic.h>

static atomic_flag unraisedFlags[SIGUSR2 + 1] = { ATOMIC_FLAG_INIT };
static sig_t savedHandler[SIGUSR2 + 1];

static atomic_flag emptyFlag = ATOMIC_FLAG_INIT;

void initHandler()
{
	for (int i = 0 ; i < SIGUSR2 + 1 ; ++i)
	{
		savedHandler[i] = NULL;
		unraisedFlags[i] = emptyFlag;
	}
}

static void theHandler(int sigNum)
{
	if (sigNum > 0 && sigNum <= SIGUSR2)
	{
		atomic_flag_clear(&unraisedFlags[sigNum]);
	}
}

static Result validateSigNum(int sigNum)
{
	// TODO: Handle SIG_DFL and SIG_IGN
	if (sigNum <= 0 || sigNum > SIGUSR2)
	{
		return CSIGNAL_INVALID;
	}
	if (savedHandler[sigNum] != NULL)
	{
		return CSIGNAL_ALREADY_HANDLED;
	}
	return CSIGNAL_OK;
}

Result setHandler(int sigNum)
{
	// Make sure the signal is in range
	Result res = validateSigNum(sigNum);
	if (res != CSIGNAL_OK) return res;

	atomic_flag_test_and_set(&unraisedFlags[sigNum]);
	sig_t oldHandler = signal(sigNum, theHandler);
	if (oldHandler == SIG_ERR)
	{
		res = CSIGNAL_INVALID;
	}
	else
	{
		savedHandler[sigNum] = oldHandler;
	}
	return res;
}

Result restore(int sigNum)
{
	if (sigNum <= 0 || sigNum > SIGUSR2)
	{
		return CSIGNAL_INVALID;
	}
	if (savedHandler[sigNum] != NULL)
	{
		signal(sigNum, savedHandler[sigNum]);
		savedHandler[sigNum] = NULL;
	}
	return CSIGNAL_OK;
}

bool signalRaised(int sigNum)
{
	// validateSigNum only returns OK if we are not monitoring it. To check if a
	// signal is raised we do need to be monitoring it.
	if (validateSigNum(sigNum) != CSIGNAL_ALREADY_HANDLED)
	{
		return false;
	}
	bool ret = !atomic_flag_test_and_set(&unraisedFlags[sigNum]);
	return ret;
}

bool isMonitoring(int sigNum)
{
	if (validateSigNum(sigNum) != CSIGNAL_OK)
	{
		return false;
	}
	return savedHandler[sigNum] != NULL;
}

int resultAsInt(Result result)
{
	return result;
}
