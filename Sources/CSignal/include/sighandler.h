//
//  sighandler.h
//  
//
//  Created by Jeremy Pereira on 13/07/2021.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef sighandler_h
#define sighandler_h

#include <stdbool.h>
#include <signal.h>

typedef enum Result
{
	/// Successful API call
	CSIGNAL_OK = 0,
	/// Invalid signal number
	CSIGNAL_INVALID,
	/// Try to handle a signal we are already handling
	CSIGNAL_ALREADY_HANDLED
} Result;

/// Initialise all the data structures needed for the idgnal handler
extern void initHandler();

/// Sets a signal handler for a given signal.
///
/// If set, and the signal is raised, a flag is set that can be retrieved
/// with `signalRaised()`
///
/// You can use this function to set `SIG_DFL` and `SIG_IGM` too.
/// @param signal The signal number for which to set a handler.
/// @return CSIGNAL_OK or an error result.
extern Result setHandler(int signal);

/// Restore a signal handler back how it was before we set it.
/// @param signal signal to restore
/// @return CSIGNAL_OK if successful or CSIGNAL_INVALID if the signal is invalid.
extern Result restore(int signal);

/// Test to see if a certain signal has been raised
///
/// This will reset the flag if you call it twice the second time the flag
/// will be false, unless
/// @param signal The signal to test
/// @return true if a signal has been raised
extern bool signalRaised(int signal);

/// Are we monitoring this signal
///
/// @param sigNum The signal to check
/// @return true if we are monitoring the signal
extern bool isMonitoring(int sigNum);

/// Convert a Result to an int
///
/// Swift's type checking stops us from using Result directly as an int
/// @param result The result value
/// @return the result as an int
extern int resultAsInt(Result result);

#endif /* signhandler_h */
