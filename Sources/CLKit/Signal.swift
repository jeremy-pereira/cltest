//
//  Signal.swift
//  
//
//  Created by Jeremy Pereira on 13/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


#if os(Linux)
    import Glibc
#else
    import Darwin
#endif
import CSignal

private var isInitialised = false


/// An enumeration represnting the various signals that can be raised
///
/// This enumeration is defined by the macOS 11 man page for `signal(3)`
public enum Signal: Int32, CaseIterable
{
	case hup = 1
	case int = 2
	case quit = 3
	case ill = 4
	case trap = 5
	case abrt = 6
	case emt = 7
	case fpe = 8
	case kill = 9
	case bus = 10
	case segv = 11
	case sys = 12
	case pipe = 13
	case alrm = 14
	case term = 15
	case urg = 16
	case stop = 17
	case tstp = 18
	case cont = 19
	case chld = 20
	case ttin = 21
	case ttou = 22
	case io = 23
	case xcpu = 24
	case xfsz = 25
	case vtalrm = 26
	case prof = 27
	case winch = 28
	case info = 29
	case usr1 = 30
	case usr2 = 31

	/// Are we monitoring this signal
	public var monitoring: Bool
	{
		get { CSignal.isMonitoring(rawValue) }
	}

	/// Start monitoring this signal
	///
	/// This is not thread safe
	/// - Throws: if the signal is invalid or we are already monitoring it
	public func monitor() throws
	{
		// TODO: This is not thread safe
		if !isInitialised
		{
			CSignal.initHandler()
			isInitialised = true
		}

		let status = CSignal.setHandler(rawValue)
		switch status
		{
		case CSIGNAL_INVALID:
			throw Error.invalidSignal
		case CSIGNAL_ALREADY_HANDLED:
			throw Error.alreadyHandled
		case CSIGNAL_OK:
			break
		default:
			throw Error.unexpected(Int(CSignal.resultAsInt(status)))
		}
	}

	/// Stop monitoring this signal
	/// - Throws: if the signal is invalid
	public func stopMonitoring() throws
	{
		let status = CSignal.restore(rawValue)
		switch status
		{
		case CSIGNAL_INVALID:
			throw Error.invalidSignal
		case CSIGNAL_OK:
			break
		default:
			throw Error.unexpected(Int(CSignal.resultAsInt(status)))
		}
	}

	/// Was a signal raised
	///
	/// This also resets the flag so calling it a second time will return `false`
	/// unless the signal was raised again
	/// - Returns: true if a signal was raised
	public func wasRaised() -> Bool
	{
		return CSignal.signalRaised(self.rawValue)
	}

	/// Run a closure while monitoring a signal
	///
	/// This function starts monitoring the signal, unless it is already being
	/// monitired, then runs the closure, passing the signal as its parameter.
	/// when the closure returns, if the signal was not already being monitored,
	/// monitoring is turned off.
	///
	/// This is not thread safe
	///
	/// - Parameter monitoredBlock: The closure to run while monitoring the
	///                             signal. It takes one parameter which is the
	///                             signal being monitored.
	/// - Throws: if the signal is invalid
	public func monitor(_ monitoredBlock: (Signal) throws -> ()) throws
	{
		let status = CSignal.setHandler(rawValue)
		switch status
		{
		case CSIGNAL_OK, CSIGNAL_ALREADY_HANDLED:
			break
		case CSIGNAL_INVALID:
			throw Error.invalidSignal
		default:
			throw Error.unexpected(Int(CSignal.resultAsInt(status)))
		}
		var errorFromBlock: Swift.Error? = nil
		do
		{
			try monitoredBlock(self)
		}
		catch
		{
			errorFromBlock = error
		}
		if status != CSIGNAL_ALREADY_HANDLED
		{
			try stopMonitoring()
		}
		if let errorToThrow = errorFromBlock { throw errorToThrow }
	}
}

public extension Signal
{
	/// Errors that might be raised by a signal
	enum Error: Swift.Error
	{
		case invalidSignal
		case alreadyHandled
		case unexpected(Int)
	}
}
