//
//  CommandLineApp.swift
//  
//
//  Created by Jeremy Pereira on 13/07/2021.
//
//  Copyright (c) Jeremy Pereira xxx2021x
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import CSignal

public struct CommandLineApp
{
	public init() {}

	public func run()
	{
		do
		{
			try Signal.int.monitor()
			for i in 0 ..< Int.max
			{
				guard !Signal.int.wasRaised() else { break }
				if i % 1000000 == 0
				{
					print("Iterated \(i) times")
				}
			}
			print("\nCaught ctrl-c and Terminated")
		}
		catch
		{
			print("\(error)")
		}
	}
	public func runWithBlock()
	{
		do
		{
			try Signal.int.monitor
			{
				for i in 0 ..< Int.max
				{
					guard !$0.wasRaised() else { break }
					if i % 1000000 == 0
					{
						print("Iterated \(i) times")
					}
				}
			}
			print("\nCaught ctrl-c and Terminated")
		}
		catch
		{
			print("\(error)")
		}
	}
}

